package tree.read;

/**
 * File: TreeFileReader.java
 * @author josephhernandez5
 * ------------------------
 *
 */



import tree.Tree;
import tree.util.StringUtil;
import java.io.*;
import java.util.ArrayList;

public class TreeFileReader {

   public static final String NULL = "null";
   private static final char[] LEVEL_INDICATORS = new char[]{'+','*','=','-'};
   private static final String COMMENT = "//";

   public TreeFileReader(String treeFilename) {
      if (!treeFilename.endsWith(".tree")) {
         System.out.println("TreeReader.error: wrong file type (exprected .tree).");
         System.exit(-1);
      }

      trees = new ArrayList<>();
      lastTreeAccessed = -1;
      try {
         BufferedReader br = new BufferedReader(new FileReader(new File(treeFilename)));
         Tree currentTree = null;
         while (true) {
            String line = br.readLine();
            if (line == null) {
               if (currentTree != null) {
                  trees.add(currentTree);
               }
               break;
            }

            line = line.trim();
            if (line.isEmpty() || line.startsWith(COMMENT)) {
               continue;
            }

            char levelIndicator = line.charAt(0);
            String value = line.substring(1);
            int levelNum = getLevelNumber(levelIndicator);

            if (levelNum == 0) {
               if (currentTree != null) {
                  trees.add(currentTree);
               }
               int tupleStart = value.indexOf('(');
               String root = value.substring(0, tupleStart);
               String tuple = value.substring(tupleStart);
               ArrayList<String> levelNames = StringUtil.tupleToStringList(tuple);
               currentTree = new Tree(root, levelNames);
            } else {
               boolean success = currentTree.addNode(levelNum, value);
               if (!success) {
                  throw new Exception(line);
               }
            }
         }
         br.close();
      } catch (Exception e) {
         System.out.println("Error reading tree file at line: " + e.getMessage());
         System.exit(-1);
      }
   }

   private int getLevelNumber(char ch) {
      for (int i = 0; i < LEVEL_INDICATORS.length; i++) {
         if (LEVEL_INDICATORS[i] == ch) { return i; }
      }
      return -1;
   }

   public Tree getTree(String rootValue) {
      for (Tree tree : trees) {
         if (tree.getRoot().getValue().equals(rootValue)) {
            return tree;
         }
      }
      return null;
   }

   //Instance Variables.
   private ArrayList<Tree> trees;
   private int lastTreeAccessed;
}
