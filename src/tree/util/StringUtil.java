/**
 * File:
 * @author josephhernandez5
 * ------------------------
 * SHOULD PROBABLY CONDENSE ALL OF THE STRINGUTIL FILES INTO ONE. LIKE SERIOUSLY.
 */

package tree.util;

import java.util.ArrayList;

public class StringUtil {

   public static ArrayList<String> tupleToStringList(String tuple) {
      //Tuple must have form (str1,str2,str3). Whitespace ignored.
      ArrayList<String> list = new ArrayList<>();
      tuple = tuple.substring(1);
      while (!tuple.isEmpty()) {
         int comma = tuple.indexOf(',');
         int end = tuple.indexOf(')');
         int nextDelimiter = (comma == -1 ^ comma > end) ? end : comma;
         String value = tuple.substring(0, nextDelimiter).trim();
         if (!value.isEmpty()) {
            list.add(value);
         }
         tuple = tuple.substring(nextDelimiter + 1);
      }
      return list;
   }

}
