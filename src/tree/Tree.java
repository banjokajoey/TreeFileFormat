package tree;

/**
 * File: Tree.java
 * @author josephhernandez5
 * ------------------------
 * A tree can have only one root.
 * Level names are immutable.
 */

import java.util.*;
import util.list.ListUtil;

public class Tree {

   public Tree(String root, ArrayList<String> levelNames) {
      this.levelNames = levelNames;
      this.root = new Node(0, root);
      treeNavigator = new Stack<Node>();
      treeNavigator.push(this.root);
   }

   public Tree(Node root, ArrayList<String> levelNames) {
      this.root = root;
      this.levelNames = levelNames;
      treeNavigator = new Stack<Node>();
      treeNavigator.push(this.root);
   }

   public Node getRoot() {
      return root;
   }

   public Tree getTreeFromNode(String levelName, Node n) {
      ArrayList<String> newLevelNames = ListUtil.getSubList(levelNames, getLevelNumberedBy0(levelName));
      return new Tree(n, newLevelNames);
   }

   public boolean addNode(int levelNum, String value) {
      int currentLevel = treeNavigator.size() - 1;
      int levelDifference = levelNum - currentLevel;
      if (levelNum > 0 && levelDifference <= 1) {
         Node n = new Node(levelNum, value);
         while (levelDifference++ <= 0) {
            treeNavigator.pop();
         }
         n.addParent(treeNavigator.peek());
         treeNavigator.peek().addChild(n);
         treeNavigator.push(n);
         return true;//Indicates success adding node.
      }
      return false;//Indicates failure adding node.
   }

   public int getLevelNumberedBy0(String levelName) {
      return levelNames.indexOf(levelName);
   }

   public int getLevelNumberedBy1(String levelName) {
      return levelNames.indexOf(levelName) + 1;
   }

   public ArrayList<Node> getNodes(String levelName) {
      int levelNum = getLevelNumberedBy1(levelName);
      ArrayList<Node> nodes = new ArrayList<>();
      this.getNodesAtLevelRecur(root, levelNum, nodes);
      return nodes;
   }

   private void getNodesAtLevelRecur(Node n, int levelNum, ArrayList<Node> nodes) {
      if (n.levelNum == levelNum) {
         nodes.add(n);
      }
      if (n.levelNum >= levelNum) {
         return;
      }
      for (Node child : n.getChildren()) {
         getNodesAtLevelRecur(child, levelNum, nodes);
      }
   }

   public Node getNode(String levelName, String value) {
      int levelNum = getLevelNumberedBy1(levelName);
      return getNodeAtLevelRecur(root, levelNum, value);
   }

   private Node getNodeAtLevelRecur(Node n, int levelNum, String value) {
      if (n.levelNum == levelNum && n.value.equals(value)) {
         return n;
      }
      Node result = null;
      Iterator<Node> it = n.getChildren().iterator();
      while (it.hasNext() && result == null) {
         result = getNodeAtLevelRecur(it.next(), levelNum, value);
      }
      return result;
   }

   @Override
   public String toString() {
      ArrayList<String> strings = new ArrayList<String>();
      this.getNodeStringsRecur(root, strings);

      StringBuilder s = new StringBuilder();
      s.append("Level names: ");
      s.append(levelNames);
      s.append('\n');
      for (String str : strings) {
         s.append(str);
         s.append('\n');
      }
      return s.toString();
   }

   //toString() helper function.
   private void getNodeStringsRecur(Node node, ArrayList<String> strings) {
      strings.add(node.toString());
      for (Node child : node.children) {
         getNodeStringsRecur(child, strings);
      }
   }

   //Instance Variables.
   private String treeName;
   private ArrayList<String> levelNames;
   private Node root;
   private Stack<Node> treeNavigator;

   public class Node {

      public Node(int levelNum, String value) {
         this.levelNum = levelNum;
         this.value = value;
         this.parent = null;
         this.children = new ArrayList<Node>();
      }

      public void addParent (Node n) {
         parent = n;
      }

      public void addChild(Node n) {
         children.add(n);
      }

      public String getFirstChildValue() {
         Node firstChild = getFirstChild();
         return (firstChild == null) ? null : firstChild.getValue();
      }

      public Node getFirstChild() {
         return getChild(0);
      }

      public Node getChild(int num) {
         return children.get(num);
      }

      public ArrayList<Node> getChildren() {
         return children;
      }

      public int    getTreeLevel() { return levelNum; }
      public String getValue()     { return value;    }

      @Override
      public String toString() {
         return levelNum + "-" + value;
      }

      //Instance Variables.
      private int levelNum;
      private String value;
      private Node parent;
      private ArrayList<Node> children;
   }
}